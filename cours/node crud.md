#CRUD 
Crud c'est a dire les opérations gestionnaire de paquet c'est l'equivalence de playstore mais en node.

## c'est quoi CRUD:
ça veut dire **Create**, **Read**, **Update**, **Delete**, si jamais on nous demander de faire un CRUD sur quelque chose 
ça signifie de faire toutes ces opérations. C'est très important parceque la plupart du temps en informatique
on sera amener à faire du CRUD 

````
 C = create:
C'est quand on veut créer une ressource 

* app.post('/hostels/:idHostel', function (req,res){
    const hostelCreated = parseInt(req.params.idHostel);
    const createRoom = req.body;
    const roomCreated = addRoom(createRoom,hostelCreated);
    res.status(201).send(roomCreated);
 
 R = read:
C'est pour lire quelque chose, guetter


* app.get('/hostels/:id', function (req,res) {
    const id =  parseInt(req.params.id);
    console.log(id);
    res.send(getHotelById(id));
});

 U = apdate:
C'est pour mettre à jour

* app.put('/hostels/:id',function (req,res) {
    const createdHostel = req.body;
    const idHostel = parseInt(req.params.id);
    const hostelCreate = putHostel(createdHostel,idHostel);
    res.send(hostelCreate);
});

 D = delete:
C'est pour supprimer 

* app.delete('/hostels/:id',function (req,res) {
    const id = parseInt(req.params.id)
    console.log(id);
    res.send(removeHostel(id));

````
### Comment activer Node:
On doit télécharger la verion qu'on veut sur le site officiel du node en tapant ``node.js``.
Après pour vérifier que Node marche on va dans notre terminal sur Webstorm, on tape ``node --version`` 
la commande nous renvoit la version qui est installée sur notre ordinateur. Il est important de s'assurer que dans notre 
console on est bien à la racine de notre projet avant d'éxécuter les commandes.
En node on a un système qui permet de télécharger des minis applications, ces outils nous 
permettent de travailler en temps que Developpeur et pour mettre en marche la possibilité de lancer ces applications, 
on utilise ``Npm``.   
### NPM: node package manager:
npm est le gestionnaire de paquets officiel de Node.js. Depuis la version 0.6.3 de Node.js, npm fait partie de l'environnement et est donc automatiquement installé par défaut3. npm fonctionne avec un terminal et gère les dépendances pour une application. Il permet également d'installer des applications Node.js disponibles sur le dépôt npm.
### Npm init:
Quand on commence un nouveau projet et que y'a pas de ``dependencies`` installés il faut écrire ``mpn init`` 
dans notre console pour que les fichiers ``packages.json`` soient installés et après on écrit  ``npm i -S express``
si on veut les stocker dans les fichiers Dependencies ou ``npm i - D express`` si on les veut dans les devDependencies  

sur terminal webstorm on tape "npm init => entrer" et quand on ouvre package.json on trouve deux type d'information:
* devdependencies: sont les applications qu'on a besoin pour developper.

* dependencies: se sont les applications qu'on va avoir utiliser par notre application finale par le client.
### Express:
Express est une infrastructure d'applications Web Node.js minimaliste et flexible qui fournit un ensemble de fonctionnalités
 robuste pour les applications Web et mobiles. pour le faire marcher on va dans le site ``Express`` et on a toutes les informations pour 
 pouvoir l'utiliser. On choisit un port pour pouvoir écouter, parcequ'il ya différents ports dans notre ordinateur,
 en gros ce sont les endroits disponibles qui permettent à notre ordinateur d'etre accessible partout par internet.
 Après avoir choisit notre port, on vient sur notre terminal, pour lancer notre fichier ``index.js``
 , le terminal nous confirme qu'il écoute sur le port qu'on a choisit et après on va dans le navigateur et on tape 
 ``localhost`` suivi du numéro de notre port pour nous aider à écouter les servers qui sont sur notre ordinateur.
A chaque fois qu'on modifie quelque chose sur notre code JS on doit redemarrer notre server dans le terminal et pour se faire on tape 
``ctrl + C`` pour éteindre notre server et après on éxécute ``Node index.Js``   
 
 **Req**: il nous permet de récupérer la requete qui est faite par l'utilisateur
 
 **Res**: c'est l'outil qu'on va utiliser pour répondre à l'utilisateur, c'est pour renvoyer un résultat 



  
  

