# Boucle for:

La boucle for est un outil qui va nous permettre de générer des données c'est à dire de faire des boucles 
sans arret, sauf que c'est nous qui allions décider sur quoi il boucle contrairement aux autres opérateurs comme 
``map, forEach`` qui bouclent sur les éléments d'un tableau .Pour ce faire on a besoin de la pseudo
 randomisation, normalement on dit nombre aléatoire mais on l'appelle ``pseudo`` pour s'habituer parcequ'en
  informatique l'aléatoire n'existe pas.
  
## Les différentes parties d'une boucle for:
   
   Dans une boucle for on a deux premières parties:
   
 **La première partie c'est la partie dite conditionnelle**: c'est dans cette partie qu'on va exprimer les conditions
 dans laquelle la boucle for va tourner.
 
  **la deuxième partie c'est la fonction de callback** c'est là qu'on va éxécuter ce qu'il y a dans la boucle for c'est l'équivalent de la fonction 
  de ``callback`` qu'on a dans un foreach, map etc.. 
  
  et dans la première partie on a 3 sous parties qui sont optionnelles, on peut très bien faire une boucle for sans l'une
  de ces 3 parties
  
  **La première partie:** c'est la partie dite ``initialisation`` elle vient avant la première virgule, c'est une expression
  de code qui  est évaluée au démarrage de la boucle for
  
  **La deuxième partie:** Elle vient après la première virgule, elle sert à savoir dans quelle condition va continuer à jouer la boucle for mais 
  par contre ce n'est pas du code qui va ètre éxécuter elle nous renvoit un prédicat soit ``false`` soit ``true``
  mais on ne les écrit pas parceque si c'est false la boucle s'arrete et si c'est true la boucle continue de tourner.
  Donc si on écrit true la boucle ne s'arretera jamais de tourner et sion écrit false la boucle ne démarrera jamais
  sauf la première partie qui s'éxécutera une fois 
  
  **La triosième partie:** C'est la partie qui vient après la seconde virgule et c'est un bout de code qui
   est éxécutée à chaque fois qu'on fint la boucle for en général on y met
    ``i++``, ``i--``, ``i+=2``, 
    ``i-=2`` mais ça peut ètre n'importe quoi.
   ```
   ``i++`` c'est l'abréviation  de ``i = i+1`` et non ``i+1`` parceque c'est différent
   ``i--`` c'est l'abréviation  de ``i = i-1``
   ``i+=2`` c'est l'abréviation  de ``i = i+2``
   ```
   
### Exemple de boucle for:

![image](../cours/assets/boucleForNames.png)

* exemple d'incrementation:

![image](../cours/assets/incrementer.png)

* exemple de decrementation:

![image](../cours/assets/decrementation.png)
 
 
 exemple:
 
 ![exercice](../cours/assets/exo%20boucle%20for%20eleve.png)
 
 
### Le modulo %:
C'est pour savoir si le nmobre est divisible par un nombre, si oui il va étre égal à zéro sinon c'est 1.

![image](../cours/assets/modulo.png)


Exemple de modulo exercice ``FizzBuzz``:
```
const number = [];
for (let i = 1; i < 200; i++) {
    if ((i % 3 === 0) && (i % 5 === 0)) {
        number.push('fizzBuzz')
    } else if (i % 3 === 0) {
        number.push('fizz')
    } else if (i % 5 === 0) {
        number.push('Buzz')
    } else number.push(i)
}
console.log(number);
``` 

* Exemple Nombre premier/ non premier:

![image](../cours/assets/exemple%20modulo+definition.png)