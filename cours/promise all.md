# Promise

 Après on invoque firestore c'est à dire qu'on va stoquer ``admin.firestore()`` qui est firestore dans une variable ``db`` 
 qui va nous permettre d'acceder en raccourci aux fonctions de firestore.
 
``db:`` veut dire qu'on va travailler dans db (la base de donnée)  

``collection:`` c'est pour expliquer dans quelle collection on travaille

``.data:`` c'est dire à l'ordinateur de renvoyer que les data de la base de données 

``.catch():`` il sert à gérer les erreurs 

``Promis.all:`` permet de jouer toutes les promises en meme temps 

Après ``collection`` il peut y avoir ``.doc`` si on veut travailler dans un document en particulier sinon on peut travailler directement dans la collection.
Si on veut travailler directement dans la collection on met un verbe d'action directement comme (add, get, delete, set...) ça va le déclencher directement 
dans la collection.

![image](../cours/assets/collection.png)

Mais si on met collection et document ça déclenchera le verbe d'action sur le document 
``collection.doc:`` permet de cibler sur quoi on veut travailler 

![image](../cours/assets/collectionDoc.png)

## Comment traiter les données:
Comme on travaille avec des promesses à l'ancienne, les fonctions qu'on a dans node sont des fonctions asynchrone c'est à dire qu'elles vont se déclencher 
et avoir 2 types d'effets:

**Le premier effet:** c'est que le code va se jouer toute suite

**le deuxieme effet:** c'est que le code va etre jouer avec du retard 

Quand on a une ``promise`` ce qu'il faut faire pour etre sure qu'elle soit bien jouée c'est faire ``.then()`` et y mettre ce qu'on veut jouer c'est 
à dire ``(a) => res.send(a.data())`` sinon on aura pas de réponse la fonction ne se jouera jamais parceque l'ordinateur il va très vite il lance 
 la fonction et la met en attente et passe à la fonction suivante meme si on fait un console.log  
 
Quand on fait des promises la première chose à faire c'est de mettre le ``.then`` et les ``.catch`` directement ça nous évitera d'oublier de le faire.
Quand on a des promises dans des promises le catch qui se jouera sera le catch final c'est le plus important donc ça ne sert à rien d'avoir beaucoup de catch  

