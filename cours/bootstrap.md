#**BOOTSTRAP**




##C'est quoi le bootsrap?

- Bootstrap est une collection d'outils utiles à la création du design (graphisme, animation et interactions avec la page dans le navigateur, etc.) de sites et d'applications web. C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons, outils de navigation et autres éléments interactifs, ainsi que des extensions JavaScript en option. C'est l'un des projets les plus populaires sur la plate-forme de gestion de développement GitHub.
  
##Les balise de referencement:
- Les balise de referencement fonctionne comme des < div > mais elles servent aux moteurs de recherche.
- Si l'on met plusieurs balise de referencement comme section le moteur de recherche va penser que l'on traite de X sujet different.
+ <section> </section> 
- ``section`` permet de disposer le site en plusieurs categories aidant au referencement. 

- **< header > < /header >**  
    - `header` un titre ou un paragraphe explicatif ou informatif du sujet.
    
- **< footer > < /footer >**
    - `footer` permet de rediriger vers une autre page ou bien vers une autre partie de la page ou du site. Mais permet de sité les sources et autres informations.

## **Bootstrap**

### **Introduction**
Bootstrap est un framework qui permet de d'organiser ou d'ajouter des fonctions a celle-ci facilement et rapidement. On utilise alors des outils deja creer par d'autres personnes.
- Ils existent plusieurs framework comme
    - Bootstrap
    - Foundation
    - Stylus
    - etc...

### **Comment l'utiliser ?**

Pour commencer il faut aller sur le site : https://getbootstrap.com/docs/4.3/getting-started/introduction/  
Une fois sur le site Bootstrap, on a deux versions une avec seulement le CSS et l'autre avec du JavaScript.

![bootstrap](bootstrap.PNG)

Coller la baliser sous le < head >  

Ensuite dans la categorie `Components` puis rechercher et regarder les categories en fonctions de ce que l'on veux.

![components](Components%20boostrap.png)

### **Container**

Un site qui utlise Bootstrap doit obligatoirement etre sous un `Container`.  
Un `container` est une < div > principale dans laquelle Bootstrap nous oblige a mettre tous sont contenu.  
```javascript
<div class="container">
```
Un `container` basique permet de centrer le contenu de la page automatiquement au centre en fonction de la taille de la page, et on ne avoir **qu'un seul container** par page.  

```javascript
<div class="container-fluid">
```
Un `container-fluid` permet au contenu de prendre toute la place sur la page, mais tout en laissant un petit espace a gauche (goutière).

###**row et col**

`col` et `row` permettent de diviser les espaces de la page en ligne (`row`) et en colonne (`col`), chaque ligne et chaque colonne sont séparer par de petit espaces pour les distinguer.  
Un `row` doit etre toujours suivi d'un `col`
```javascript
<div class="row">
```
```javascript
<div class="col-12>
```
`col` peut aller jusqu'a 12 maximum

On peux aussi modifier le `col` pour s'adapter a la taille de la page (responsive) grace a cette syntax :  
```javascript
<div class="col-12 col-md-4">
```
On peux aussi utiliser avec les `col` et les `row` pour afficher plusieurs information ou objet sur une meme ligne.

`col-12 col-md-4` veut dire que quand la `col` est inferieur a `md` (medium) de taille de page, le contenu va s'adapter la taille de la page.

Pour centrer le contenu au centre de la `row` ont ajoute `justify-content-center` a `row`
```javascript
<div class="row justtify-content-center">


