#Merge request
****

##Introduction Gitflow:

- Gitlow workflow est une conception de flux de travail git.
- le flux de travail gitflow definit un model de branchement conçu autour de la version du projet, il convient parfaitement aux projets dont le cycle de publications est planofié.

*****
## Les commits 

Les commits sont important et indispensable pour toujours avoir une trace du code créé.
Cela prouve également le professionnalisme du développeur.


## Comment ça fonctionne:

![Image-1024x716](/uploads/10a8e15353320ac34bcb4d47466ef82b/Image-1024x716.png)

## Developper est maitriser des branches:

- au lieu d'une seule master branche, ce flux de travail utilise deux branches pour enregistrer l'historique des versions officielles et developsert de branche d'integration pour les fontionnalités.
Il est egalement pratique de baliser tous les commits de la master branche vide et à la transmettre au serveur.

#### La master 

La master est la version officielle d'un projet. C'est la version qui va déployé sur CDN.
Elle ne doit *jamais être modifier*  

Chaque copie de se premier fichier s'appelle une branche.

#### Les branches 

Les branches son finalement les version que vont utiliser les développeurs pour créer leurs partie du code.
Elles alimenteront la MASTER quand ils auront terminé.  
C'est ce qu'on appel MERGE REQUEST.


Il est possible 


##### Comment créer une branche

1. Sur webstorm aller dans Git;master en bas à droite ou aller dans VCS 
cliquer sur VCS Opération popup

2. Pour donner un nom à la branche commencer par feature ensuite mettre '/' l'objet du travail écrit en camel case

3. Démarer le travail en question et commit

#####Comment changer de branche

Pour changer de branche il faut revenir sur Git;feature ou master et puis checkout

#### Valider avec des Merges 

1.Pour Merger il faut retourner sur Master est faire un checkout

2. Ensuite pour aller dans VCS; cliquer sur GIT puis merge changes

3. il y a le choix entre
    *feature change ..., (stocker sur l'ordi)
    *remote origin feature change (stocker sur gitlab)
    
4. cliquer sur feature change header puis merger

5. Faire un commit puis push

6. Penser à delete la branche


###Valider avec des Merges request

Le Merge request permet de faire valider sont travail auprès d'une tierce personne.
Notamment son manager.

1. Penser à faire Commit/Push 

2. Aller dans Gitlab et ce mettre sur son projet 

3. Cliquez sur Merge reauest à gauche

4. Cliquez sur demande

5. Cliquez sur create merge request

6. Change branches


#### Les conflits 

Lorsque deux personnes modifie la même partie du code; webstorm détecte un conflit. 
Pour résoudre le conflit webstorm affiche à l'écran 2 parties detectées comme conflictuelle et 
une autre fenêtre avec la version master.
Le responsable n'a plus qu'a décider la version qu'il veut sauvegarder.
Un seul version ne peut être validé.
Ensuite penser à pusher
La plus part du temps ce n'est pas au responsqble de résoudre les conflits mais plus tôt qu membre de l'équipe de ce
mettre d'accord


