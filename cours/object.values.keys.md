#object values/ object keys:

##object values:
*****

La méthode Object.values() renvoie un tableau contenant les valeurs des propriétés propres énumérables d'un objet dont l'ordre est le même que celui obtenu avec une boucle for...in (la boucle for-in est différente car elle parcourt également les propriétés héritées).
 
=> on utilise **object values** pour changer quelque chose dans la valeur de la propriété:

* syntax:

``````
Object.values(obj)

``````
* obj:

L'objet dont on trouve la liste des valeurs.

##object keys:

*****

La méthode Object.keys() renvoie un tableau contenant les noms des propriétés propres à un objet (qui ne sont pas héritées via la chaîne de prototypes) et qui sont énumérables.

=> on utilise **object keys** pour changer une chose dans la propriété:

* syntax:

``````
Object.keys(obj)

``````
* obj:

L'objet dont on trouve la liste des propriété.
## Transformer un objet en tableau

Dans un objet on a des clés ``key`` qui sont des attributs et des valeurs ``values`` qu'on les attribue. 
On ne peut pas transformer tout l'objet en tableau, on récupère soit que ces **clés** soit que ces **valeurs**.

**Rappels**: l'opérateur ``Delete`` permet de supprimer l'élément d'un objet quand on l'a en double.

![objet](../cours/assets/objets.png)

