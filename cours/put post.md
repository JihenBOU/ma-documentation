# Put et Post
**Rappels:**
rest: façon d'organiser les URL 
## Pour pouvoir utliser express pour le CRUD :

On copie les constantes ci-dessous à partir de la documentation de ``Express`` en cliquant sur **Guide** et puis **Routage**

```
const express = require('express');
const app = express();
```

## Pour pouvoir utliser le post et le put :
On copie les deux lignes ci-dessous tout en haut de notre code js à partir de la documentation d'express en allant 
à la barre de recherche on écrit `req.body`

```
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
```

### EXEMPLE get :

```
app.get('/hostels/:id', function (req, res) {
    const id = parseInt(req.params.id);
    const theHostel = returnHostel(id);
    res.send(theHostel);
});
```

### EXEMPLE delete :
```
app.delete('/hostels/:id', function (req, res) {
    const idHotel = parseInt(req.params.id);
    res.send(removeHostel(idHotel));
});
```

### EXEMPLE post :

```
app.post('/hostels', function (req, res) {
    const newHostel = req.body;
    const addedHostel = addHostel(newHostel);
    res.send(addedHostel);
});
```

### EXEMPLE put :

```
app.put('/hostels/:idHostel', function (req, res) {
    const newHostel = req.body;
    const indexHostel = parseInt(req.params.idHostel);
    const changedHostel = putHostel(indexHostel, newHostel);
    res.send(changedHostel);
});
```

## liste des codes Http:

pour aoir cette liste on va dans ``Google`` on tape ``liste des codes http`` dans la barre de recherche et selectionne celui de wikipédia.
Les plus fréquentes sont les codes `200`, `400`, `500`

``https://fr.wikipedia.org/wiki/Liste_des_codes_HTTP``