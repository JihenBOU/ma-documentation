# Await Async:
``await`` ``async`` remplacent les ``.then`` et les ``.catch`` dans les promises pour les faire proprement il faut systèmatiquement
commencer le code avec ``try`` et ``catch`` c'est très important. C'est une méthode différente pour gérer les promises 

```as: c'est un caster, c'est quelque chose qui change le type de quelque chose en autre choses.```

## Exemple de ``.then``:

![image](../cours/assets/tryCatch.png)

## L'equivalence en ``await async``:

Pour travailler avec ``await async`` la première chose à faire c'est mettre **async** avant **function** pour dire qu'on travaille en asynchrone 
sinon ça ne marchera pas. A partir du moment qu'on déclare travailler avec await async les then et les catch n'existent plus. Maintenant qu'on met 
``async`` la promise ne s'exécutera que si on met ``await`` devant.
C'est mieux d'utiliser await async parce que ça nous évite d'avoir des callbacks dans des callbacks, avoir des datas les unes à la suite des autres

![image](../cours/assets/await_async.png)

