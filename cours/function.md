#Function:

****

- Le mot cle pour declare une fonction c'est **Function**

- Quand on declare une function on met directement des parentéses et ensuite en met 
des crochers dans la quelle la function va exécuté:

![caputure ecran](../cours/assets/function.png)

- Une function c'est quelque chose qui déclenche des ordres.
- La function la plus standars:      
Exemple: 


    function coucou() {    

        console.log(coucou)
     }

=> Cette function elle ne renvoi rien ce qui s'appel en JS **return void** 
et il ya d'autre function qui retourne quelque chose (numbre, string...)

![image](../cours/assets/Capture%20d’écran%20function2.png)


- on peut aussi ajouter des 'if'

![image](../cours/assets/Capture%20d’écran%20function%20if.png)

- Quand on tombe sur return la function arrete de s'exucute
donc le return se met fin a une function: 
Exemple:

![image](../cours/assets/Capture%20d’écran%20return.png)

Quand on aller sur le console on trouve le resultat du return qu'on a met '0' donc le return se met fin a une function

=> Une function il a toujours une valeur de retour.

- Il ya un autre opérateur qui s'appel **restOperateur** 
et c'est le plus pratique quand on a plusieur parametre ou objet; 

- **Rest Opérator**:
  Dans une function on peut mettre autant d'arguments qu'on veut dans la parenthèse, par contre il 
  y a une norme qui ne doit pas dépasser 4 parceque sinon ça devient ingérable.
  Par contre si on ne sait pas combien de paramètres on veut déclarer il y a un opérateur qui nous 
  permet de le gérer. Il s'appelle ``rest opérator `` ``...``
  Il nous permet de prendre tous les arguments autant qu'il y'en a et le ranger dans un array donc 
  il renvoie un tableau.
  On doit avoir qu'un seul rest opérator et doit toujours etre en dernière position 
![image](../cours/assets/rest_opérator.png)
- **spreadoperateur** 

L'opérateur Spread permet à un itératif de se développer aux endroits où plus de 0 arguments sont attendus. 
Il est principalement utilisé dans un tableau de variables où plus de 1 valeurs sont attendues. Cela nous permet d'obtenir la liste des paramètres à partir d'un tableau. 
La syntaxe de l'opérateur Spread est identique à celle du paramètre Rest, mais il fonctionne complètement à l'opposé.

- **findIndex**

Une methode permet de *renvoie l'indice* du premiere element du tableau qui satisfaite une condition donner par une fonction.

* syntaxe:

`````

tab.findIndex(callback(element[, index[, tableau]])[, thisArg])

````` 
 
on peut utiliser egalement: 

**Find**

Cette methode permet de renvoie la valeur et non pas l'indice d'un des elements trouvés. 

* syntaxe:

`````

arr.find(callback(element[, index[, tableau]])[, thisArg])

`````



