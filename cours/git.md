# Git:
****

Git c'est un outil de versioning qui nour permet de crée / modifier, mettre a jour ou corriger un fichier a l'aide des ligne de commande **Git**.

## Inisialisation de Git:
- Télécharger Git de la site officielle du Git:
- Ouvrire le **Terminal**
- Taper **git --version**

![image](../cours/assets/gitVersion.png)
## L'identité
La première chose à faire après l’installation de Git est de renseigner votre nom et votre adresse de courriel.
    ``git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com``
### Vérifier vos paramètres:

     **git config --list**
user.name=John Doe
user.email=johndoe@example.com
color.status=auto
color.branch=auto
color.interactive=auto
color.diff=auto
Si vous avez besoin d’aide pour utiliser Git, il y a trois moyens d’obtenir les pages de manuel pour toutes les commandes de Git :
**git help config**
# Cloner un dépôt existant:
Si vous souhaitez obtenir une copie d’un dépôt Git existant — par exemple, un projet auquel vous aimeriez contribuer — la commande dont vous avez besoin s’appelle git clone. Si vous êtes familier avec d’autres systèmes de gestion de version tels que Subversion, vous noterez que la commande est clone et non checkout. C’est une distinction importante — Git reçoit une copie de quasiment toutes les données dont le serveur dispose. Toutes les versions de tous les fichiers pour l’historique du projet sont téléchargées quand vous lancez git clone. En fait, si le disque du serveur se corrompt, vous pouvez utiliser n’importe quel clone pour remettre le serveur dans l’état où il était au moment du clonage (vous pourriez perdre quelques paramètres du serveur, mais toutes les données sous gestion de version seraient récupérées — cf. Installation de Git sur un serveur pour de plus amples détails).

Vous clonez un dépôt avec git clone [url]. Par exemple, si vous voulez cloner la bibliothèque logicielle Git appelée libgit2, vous pouvez le faire de la manière suivante :
**git clone https://github.com/libgit2/libgit2**
Juste aprés un clonage on lance la commande **git status** pour déterminer quels fichiers sont dans quel état est la commande



### Les commandes de bases:

1- Création d'un nouveau projet:

Aller sur le terminal et taper:

               `` mkdir + Le nom du projet ``

ça va ajouter directement dans le repertoire un nouveau projet:

Pour entrer dans ce projet il sufit de taper:

                 ``cd + le nom du projet``

et je me trouve dans mon projet:

Finalement pour initialiser git on lance la commande:

                         ``git init``


![image](../cours/assets/git.png).


### Le commit:

- Pour enregistrer l'etat actuel du projet:

  1. Selection des fichiers:

              ``git add nomDeFichier```
  2. Photo des fichers:

            ``git commit -m  + "un petit commentaire pour explique pourquoi on veut commité" ``


![image](../cours/assets/gittt.png).

### Times line des commits:

Pour voir l'historique des modifications ou des commits on tape:

                  ``git log``



### Les branches:
Pour lister les branches disponible sur un projet:
                    
                    ``git branch`` ou bien ``git remote``

Pour créer un nouveau branche:

                   ``git branch + le nom de branche``



![image](../cours/assets/gitbranche.png);

Pour changer un branche:

                  ``git checkout + le nom de la branch``

![image](../cours/assets/branche:add.png)   

### Mergé:
1. selectionner la branche principal:

                    ``git checkout master``

2. Fusionner les commits:

                     ``git merge " avec la branche ou on a fait les modifications"``

3. Supression de la branche:

         ``git branch -d "nom de la branche qu'on souhaite supprimer"``


#### La colaboration avec une autre Projet:
 ## le Dépot distant et Github:

 Distribuer le projet:

 Deposons sur un espace partagé:
 Chaque membre de l'equipe peut participer au projet:

  ****
 
   **Dépot local** => **Dépot distant** => **d'autres utilisateurs**

  ****


  ### Push: 
  Pour deposer un projet :
  
  ``git push origin master``

  **Dépot local** => **Dépot Distant**
  
   ``git pull origin master``
            
  **Dépot Distant** => **d'autres utilisateurs**

*****
   **git init** => **git add** . => **git status** => **git commit -m "commentaire"** => **git push origin master**
*****

  ### D'autre ligne de commande:

  # Inspecter les modifications indexées et non indexées:
  Pour visualiser ce qui a été modifié mais pas encore indexé, tapez ``git diff`` sans autre argument :
  # Effacer des fichiers:
  ``git rm + nom du fichier``
  # Annuler des actions:
  À tout moment, vous pouvez désirer annuler une de vos dernières actions.
  ``git commit --amend``
  ### Travailler avec des dépôts distants:
  
Pour pouvoir collaborer sur un projet Git, il est nécessaire de savoir comment gérer les dépôts distants. Les dépôts distants sont des versions de votre projet qui sont hébergées sur Internet ou le réseau d’entreprise.


La commande permet d'afficher la ou les branches qui vous suivent lorsque vous commitez ``Git remote``


La commande permet d'ajouter un depot à un nomCourt pour faciliter son utilisation Ensuite, si vous voulez recuperer des information sur un URL et que vous ayez utilise git remote add vous pouvez utiliser le nomCourt lié à cette URL en utiilisant la commande git fetch nomCourt ``git remote add``

Cette commande permet de renomer de nomCourt pour à autre nom. On va reprendre l'exemple précédant. Vous pouvez toujours supprimer la réference avec la commmande git remote rm nomCourt ``git remote rename``

Git remote remove est utilisé afin de supprimer ce que le remote lui même a fait. Vous avez fait un remote et donc donné un nom à ce remote. Si vous voulez supprimer ce que vous venez de faire avec le remote, il faut juste taper git remote remove dans la console afin de supprimer cela. ``git remote remove`` (et le nom du remote)

Git remote set-url change l'url pour le remote. Si aucune url n'est déjà présente, la fonction crée une nouvelle url. Par contre si il y a déjà une url et on utilise cette fonction en ne mettant pas le bon nom, alors la console vous affichera une erreur et aucun changement ne sera effectué.``git remote set-url``
Vous pouvez aussi spécifier -v, qui vous montre l’URL que Git a stockée pour chaque nom court : ``git remote -v``
****
Maintenant, utiliser le mot-clé pb sur la ligne de commande au lieu de l’URL complète. Par exemple, si vous voulez récupérer toute l’information que Paul a mais que vous ne souhaitez pas l’avoir encore dans votre branche, vous pouvez lancer ``git fetch pb`` :













