# Comment créer des Promises?

Pour l'instant on a travaillé qu'avec des promises déjà donné comme ``collection.get, collection.set ou collection.data``.
Si mainteant on veut les fabriquer nous meme il y' a un outil qui sert à les fabriquer qui est ``new Promise`` et il renvoie 
une fonction de callback dans lequel il ya 2 arguments ``resolve`` et ``reject`` étant des fonctions tous les 2. En général dans 
les arguments nous avons des valeurs mais là nous avons des fonctions. Quand on les appelera la première qui est ``resolve`` 
va mettre fin à la promise en la donnant une valeur finale et la deuxième ``reject`` va rejeter la promise c'est donc ce qu'on 
récupèrera dans le catch.

Quand on fait des promises c'est très important de toujours penser au cas **reject**, sinon les erreurs ne seront pas catcher .
Pour qu'une promise soit interressant il faut qu'il soit différé et pour que le calcul soit différé en javaScript on utilise
 une fonction qui s'appelle ``setTimeout`` qui a le meme role que ``db.collection().then``

**Exemple de new Promise:**

![image](../cours/assets/newPromise.png)