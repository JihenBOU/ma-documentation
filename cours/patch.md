## Patch et Gestion d'erreur:

On veut juste remplacer les clés qui changent mais avant cela, on ne peut pas ajouter de nouvelles clés.
Dans un 1er temps, on vérifie si les clés sont autorisées.
Du coup, on va chercher l'objet puis la liste des clés de l'objet.
D'abord on récupère une route qui fonctionne et on introduit patch dans l'app.
Précédemment, on a définit un index dans notre liste d'objet et on va vérifier quel objet a le même id que celui reçu dans la demande.

## SYNTAXE D'UN PATCH( exemple)

``app.patch('/hostels/:id', function(req, res) {
try{`` // permet de faire nos fonctions mais dès que l'on va trouver un throw, cela va renvoyer une erreur sans faire crash(planter)tout le serveur

``const dataToChange = req.body;`` // pour récupérer le body que j'ai créer dans postman

``const id = parseInt(req.params.id);`` // récupérer le :id dans le lien et le transformer en number

``const index = hostels.findIndex(value => value.id === id);`` // .findIndex pour avoir l'index de l'hostels à modifier

``let hostelToChange = hostels[index];``

``const keysToChange = Object.keys(dataToChange);`` // Keys de mon body dans postman

``const authorizedKeys = Object.keys(hostelToChange);`` //Keys d'hostels

``const isAuthorized = keysToChange.map(key => authorizedKeys.includes(key));`` // .include permet de voir si l'object est dans le tableau et renvoie true ou false

``const isOk = isAuthorized.every(key => key);`` // every va regarder s'il y a que des true, si c'est le cas il renvoie true, sinon false

``if (!isOk){`` // si isOK n'est pas true

``throw new Error('non authorized key');}``// permet de renvoyer une erreur si la fonction n'est pas respectée

``Object.assign(hostelToChange, dataToChange);`` // fait le même travail que le spread operator en dessous, mais on a pas besoin par
 la suite de faire un put pour envoyer nos information car le assign fait le changement à la racine
 
``hostelToChange = {...hostelToChange, ...dataToChange};`` // va permettre de mixer les informations de hostelsToChange avec dataToChange donc de modifier ce que l'on veux

``putHostel(id, hostelToChange); ``// On appel notre fonction qui modifie un hostels pour appliquer les changements sur hostels
``res.send(hostelToChange);``
``} catch (e) {`` // une fois l'erreur recuperée, on renvoie une erreur sans faire crash tout le serveur ou les autres fonctions
``console.log(e.message);``
``res.status(400).send(e.message);``
}
})

## Gestion d'erreur:
Pour gérer la gestion d'erreurs on utilise throw, qui va permettre de renvoyer une erreur, par exemple:
```
throw new Error('non authorized key');
renvoie une erreur avec le texte qu'on a mis et renvoie le code 500 pour internal error.  
```

Pour contrôler, les bugs ou crash de serveur on utilise try et catch et on évite que d'autres fonctionnalités ou application du site ne crash.

```
try {  
 // nos function  
if (ok){  
throw new error('erreur de merde'); 
}  
}catch (e){  
res.status(400).send(e); // renvoie le statut 400 et renvoie le message d'erreur du throw  
}  
```