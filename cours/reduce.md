#Reduce
La méthode reduce() applique une fonction qui est un « accumulateur » et qui traite chaque valeur d'une liste (de la gauche vers la droite) afin de la réduire à une seule valeur.


````
const array [1, 2, 3, 4];
console.log(array.(reduce)reducer);
// expected output: 10

````

![image](assets/images.png)


exemple reduce chaine de caractere (string)

![image](assets/Capture%20d’écran%202019-10-07%20à%2012.10.38.png)
reduce [(pev,curr) => [ return prev + curr], '' )


Les operateus qu'ont voit jusqu'a maintenant comme:
- map (fonction callback)
- foreach
- reduce
- every
- filter

* Ils ont tous le meme mode de fonctionnement, ils commence dans un tableau et parcouris tous les element de tableau un par un. 
Et ils ont appliquer la fonction callback sur tous les elements.

- Il ya une autre type des operateurs exemple **some** dés qu'il trouve se qu'il veut il s'arrete 



