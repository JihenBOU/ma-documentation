#le cour de MARKDOWN
****


##Presentation de MARKDOWN

*****


-  Markdown est un langage de balisage léger que vous pouvez utiliser 
pour ajouter des éléments de mise en forme à des documents en texte brut. 


****

##Comment stocker le markdown avec le GITLAB
- Cree un nouveau projet dans Gitlab   
- copie le lien http clone et coller en Checkout GIT.
- Ajouter les fichier avec add   
- Personaliser le fichier avec *COMMIT*  **_ctrl/K_**     
- Transferer le travail avec *PUSH*      **_ctrl/shift/K_**

 
*****

##Les backticks           

C'est un caractere appelé le Backtick ou apostrophe inversée au début 
et à la fin du segment correspondant au code.

`mon code avec backtick`
  
****

##Comment structurer ma page MD

###LES TITRES           

Par défaut, pour rédiger un titre avec Markdown, on utilise le dièse.
On le sépare du texte avec une espace. Pour créer des sous-titres 
de hiérarchie inférieure, et donc rédigés en plus petits, 
il suffit d’insérer des dièses supplémentaires. Comme pour l’édition HTML,   
vous pourrez créer jusque 6 niveaux de sous-titres.
    
 *****
 
 ###GRAS ET ITALIQUE       
 
 ***Écrire en gras et en italique est particulièrement facile avec Markdown.
  Il suffit d’utiliser les étoiles, appelées aussi astérisques. 
  Pour écrire en italique, insérez tout simplement une étoile devant 
  et derrière l’expression le mot ou concerné. 
  Pour le gras, insérez deux étoiles avant et après. 
  Pour le gras et l’italique, vous devrez opter pour trois étoiles. 
 Une autre option consiste à utiliser les tirets bas***
 
 *****
 
 ###BARRE    
 Pour barrer un texte avec Markdown, précédez-le de deux tildes et refermez 
 la chaîne avec deux autres tildes.   
 ~~texte barre~~    
 
 *****
 
 ###PARAGRAPHES      
 
 Le langage Markdown utilise des sauts de lignes pour créer 
 des paragraphes séparés. Pour rédiger un nouveau paragraphe (balise), 
 il suffit d’insérer tout simplement une ligne vierge.   
 Si l’on souhaite insérer un saut de ligne correspondant à la balise,
 il vous faudra insérer deux espaces à la fin d’une ligne.
 
 ###LES LISTES        
 
 Si vous souhaitez établir une liste simple avec Markdown, 
 vous avez le choix entre:  
  
   + le signe plus
   - le tiret  
    ou  
   * un astérisque. 
 Ces trois options donnent le même rendu.
 Pour créer une liste numérotée, 
 il vous suffira d’inscrire un chiffre suivi d’un point.
 
 1. Liste 1
 2. Liste 2
 3. Liste 3
 
 ******
 
 
  
 
 
 
 
 
 
 
 
 
 
 


 
 
 
 
 
 





